import React from 'react';



class Box extends React.Component {
  render(){
      const {title, value } = this.props
    return (                          
            <div className="box">
                <h4>{title}</h4>
                <button onClick={this.props.dec}> -</button>
                <span> {value} </span>
                <button onClick={this.props.inc}> +</button>
            </div>
    
    
    );
  }
}

export default Box;
