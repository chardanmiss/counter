import React from 'react'

import Box from "./Box"
class BoxContent extends React.Component {

  state = { counter:20 }

  inc = () => this.setState({ counter: this.state.counter + 1})
  dec = () => this.setState({ counter: this.state.counter - 1})

  
  render() {
      return (
        <div>
            <Box value={this.state.counter} title="box first" dec={this.dec} inc={this.inc}/><br/>
        </div>
      )
  }

}

export default BoxContent